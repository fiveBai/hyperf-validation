<?php

namespace FiveBai\HyperfValidation;

use App\Constants\ErrorCode;
use Exception;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Validation\Contract\ValidatorFactoryInterface as ValidationFactory;
use Hyperf\Validation\Request\FormRequest;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class FormRequestValidator extends FormRequest
{
    const ERROR_SEPARATOR = '|';

    /**
     * @Inject()
     * @var Filter
     */
    private Filter $filterHandle;

    // 自定义方法验证
    protected $extendList = [];

    //验证规则
    protected $rules = [];

    //过滤规则
    protected $filter = [];

    //参数
    protected $parmas = [];

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    public function validator()
    {
        $this->setExtendList();
        $request = ApplicationContext::getContainer()->get(\Hyperf\HttpServer\Contract\RequestInterface::class);
        $factory = $this->container->get(ValidationFactory::class);

        $this->parmas = $request->all();
        $this->filter();

        $this->rules = $this->getRules();
        $this->extendScene();

        $validator = $factory->make($this->parmas, $this->rules, $this->messages());
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            if (empty($error)) {
                throw new Exception('验证信息为空');
            } elseif (is_string($error)) {
                if (strpos($error, self::ERROR_SEPARATOR) === false) {
                    $code = (int)(is_numeric($error) ? $error : 0);
                    $message = ErrorCode::getMessage($code) ?: '请求参数异常';
                } else {
                    $errors = explode(self::ERROR_SEPARATOR, $error);
                    $errCode = (int)(is_numeric($errors[0]) ? $errors[0] : 0);
                    $errMsg = $errors[1] ?: '请求参数异常';
                    $message = ErrorCode::getMessage($errCode) ?: $errMsg;
                }
            } else {
                throw new Exception('验证信息数据类型不支持');
            }

            throw new Exception($message ?: $error, (int)$code);
        }
    }

    /**
     * 追加某个字段的验证规则
     * @access public
     * @param string|array $field 字段名
     * @param mixed $rule 验证规则
     * @return $this
     */
    public function append($field, $rule = null)
    {
        if (empty($field) || empty($rule)) return $this;

        $rules = is_array($rule) ? $rule : explode(self::ERROR_SEPARATOR, $rule);
        if (!isset($this->rules[$field])) {
            $this->rules[$field] = implode(self::ERROR_SEPARATOR, $rules);
        }

        $ruleList = explode(self::ERROR_SEPARATOR, $this->rules[$field]);
        $ruleList = array_unique(array_merge($ruleList, $rules));
        $this->rules[$field] = implode(self::ERROR_SEPARATOR, $ruleList);
        return $this;
    }

    /**
     * 移除某个字段的验证规则
     * @access public
     * @param string|array $field 字段名
     * @param mixed $rule 验证规则 true 移除所有规则
     * @return $this
     */
    public function remove($field, $rule = null)
    {
        if (empty($field) || empty($rule) || empty($this->rules[$field])) return $this;

        $rules = is_array($rule) ? $rule : explode(self::ERROR_SEPARATOR, $rule);
        $ruleList = explode(self::ERROR_SEPARATOR, $this->rules[$field]);
        foreach ($rules as $key => $val) {
            if (!in_array($val, $ruleList)) {
                unset($rules[$key]);
            }
        }

        $ruleList = array_diff($ruleList, $rules);
        if (empty($ruleList)) {
            unset($this->rules[$field]);
            return $this;
        }

        $this->rules[$field] = implode(self::ERROR_SEPARATOR, $ruleList);
        return $this;
    }

    private function filter(): FormRequestValidator
    {
        if (!$this->filter) {
            return $this;
        }
        $params = $this->parmas;
        $paramsKey = $this->scenes[$this->getScene()] ?? [];
        foreach ($this->filter as $key => $value) {
            if (!in_array($key, $paramsKey)) {
                continue;
            }
            $more = explode(self::ERROR_SEPARATOR, $value);
            foreach ($more as $v) {
                if (!$v) {
                    continue;
                }
                if (isset($params[$key])) {
                    $params[$key] = $this->filterHandle->$v($params[$key]);
                }
                $this->parmas = $params;
            }
        }

        return $this;
    }

    private function extendScene()
    {
        $scene = $this->getScene();
        if (empty($scene)) return;

        $extendScene = 'scene_' . $scene;
        $sceneName = preg_replace_callback('/_+([a-z])/', function ($matches) {
            return strtoupper($matches[1]);
        }, $extendScene);

        if (!method_exists($this, $sceneName)) {
            return;
        }

        return call_user_func([$this, $sceneName]);
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function setExtendList(): void
    {
        if (empty($this->extendList)) {
            return;
        }

        $factory = $this->container->get(ValidationFactory::class);
        foreach ($this->extendList as $val) {
            $factory->extend($val, function ($attribute, $value, $parameters, $validator) use ($val) {
                return call_user_func([$this, $val], $attribute, $value, $parameters, $validator);
            });
        }
    }
}
