<?php

namespace FiveBai\HyperfValidation\Aspect;

use FiveBai\HyperfValidation\Annotation\Mv;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerInterface;

/**
 * @Aspect
 */
class ValidatorAspect extends AbstractAspect
{
    public $classes = [];

    public $annotations = [
        Mv::class,
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $container = ApplicationContext::getContainer()->get(ContainerInterface::class);
        $list = AnnotationCollector::getMethodsByAnnotation($this->annotations[0]);
        foreach ($list as $key => $anno) {
            if ($proceedingJoinPoint->className == $anno['class'] && $proceedingJoinPoint->methodName == $anno['method']) {
                [$class, $className] = explode("::", $anno['annotation']->class);
                $validator = new $class($container);
                $validator->scene($anno['annotation']->scene)->validator();
            }
        }
        return $proceedingJoinPoint->process();
    }
}