<?php
declare(strict_types=1);

namespace FiveBai\HyperfValidation\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;
use Hyperf\Di\Annotation\AnnotationCollector;

/**
 * @Annotation
 * @Target("METHOD")
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Mv extends AbstractAnnotation
{
    public $class;
    
    public $scene;
    
    public function __construct(...$value)
    {
        parent::__construct(...$value);
    }
}