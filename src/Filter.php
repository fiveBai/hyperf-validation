<?php

namespace FiveBai\HyperfValidation;

use Exception;

class Filter
{
    /**
     * 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
     * @param string $value 需要过滤的值
     * @return string
     */
    public function filter_script(string $value): string
    {
        $value = preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i", "&111n\\2", $value);
        $value = preg_replace("/(.*?)<\/script>/si", "", $value);
        $value = preg_replace("/(.*?)<\/iframe>/si", "", $value);
        return preg_replace("//iesU", '', $value);
    }

    /**
     * 安全过滤类-过滤HTML标签
     * @param string $value 需要过滤的值
     * @return string
     */
    public function filter_html(string $value): string
    {
        if (function_exists('htmlspecialchars')) return htmlspecialchars($value);
        return str_replace(array("&", '"', "'", "<", ">"), array("&", "\"", "'", "<", ">"), $value);
    }

    /**
     * 安全过滤类-对进入的数据加下划线 防止SQL注入
     * @param string $value 需要过滤的值
     * @return string
     */
    public function filter_sql(string $value): string
    {
        $sql = array("select", 'insert', "update", "delete", "\'", "\/\*",
            "\.\.\/", "\.\/", "union", "into", "load_file", "outfile");
        $sql_re = array("", "", "", "", "", "", "", "", "", "", "", "");
        return str_replace($sql, $sql_re, $value);
    }

    /**
     * 安全过滤类-通用数据过滤
     * @param mixed $value 需要过滤的变量
     * @return string|array
     */
    public function filter_escape($value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = self::filter_str($v);
            }
        } else {
            $value = self::filter_str($value);
        }
        return $value;
    }

    /**
     * 安全过滤类-字符串过滤 过滤特殊有危害字符
     *  Controller中使用方法：$this->controller->filter_str($value)
     * @param string $value 需要过滤的值
     * @return string
     */
    public function filter_str(string $value): string
    {
        $badStr = array("\0", "%00", "\r", '&', ' ', '"', "'", "<", ">", "   ", "%3C", "%3E");
        $newStr = array('', '', '', '&', ' ', '"', '\'', "<", ">", "   ", "<", ">");
        $value = str_replace($badStr, $newStr, $value);
        return preg_replace('/&((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $value);
    }

    /**
     * 私有路劲安全转化
     *  Controller中使用方法：$this->controller->filter_dir($fileName)
     * @param string $fileName
     * @return string
     */
    public function filter_dir(string $fileName)
    {
        $tmpName = strtolower($fileName);
        $temp = array(':/', "\0", "..");
        if (str_replace($temp, '', $tmpName) !== $tmpName) {
            return false;
        }
        return $fileName;
    }

    /**
     * 过滤目录
     *  Controller中使用方法：$this->controller->filter_path($path)
     * @param string $path
     * @return string
     */
    public function filter_path(string $path): string
    {
        $path = str_replace(array("'", '#', '=', '`', '$', '%', '&', ';'), '', $path);
        return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
    }

    /**
     * 过滤PHP标签
     *  Controller中使用方法：$this->controller->filter_php_tag($string)
     * @param string $string
     * @return string
     */
    public function filter_php_tag(string $string): string
    {
        return str_replace(array(''), array('<?', '?>'), $string);
    }

    /**
     * 安全过滤类-返回函数
     *  Controller中使用方法：$this->controller->filter_str_out($value)
     * @param string $value 需要过滤的值
     * @return string
     */
    public function filter_str_out(string $value): string
    {
        $badStr = array("<", ">", "%3C", "%3E");
        $newStr = array("<", ">", "<", ">");
        $value = str_replace($newStr, $badStr, $value);
        return stripslashes($value); //下划线
    }

    /**
     * @throws Exception
     */
    public function __invoke()
    {
        // TODO: Implement __invoke() method.
        throw new Exception("function not exit", 0);
    }
}
